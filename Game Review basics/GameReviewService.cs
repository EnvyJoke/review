﻿using System;
using System.Collections.Generic;

namespace Game_Review_basics
{
    public class GameReviewService
    {
        private List<GameReview> gameReviews = new List<GameReview>();

        public void Add(GameReview gameReview) 
        {
            gameReviews.Add(gameReview);
        }

        public void Delete(int id) 
        {
            if (gameReviews.Count == 0)
            {
                Console.WriteLine("Empty list");
                return;
            }

            int index = gameReviews.FindIndex(x => x.Id == id);

            gameReviews.RemoveAt(index);
        }

        public void Update(int id) 
        {
            var gameReview = gameReviews.Find(x => x.Id == id);

            int index = gameReviews.FindIndex(x => x.Id == id);
            gameReviews.RemoveAt(index);

            gameReview.Id = id;




            gameReview = EnterData1();
            gameReviews.Insert(index, gameReview);
        }

        public void Get(int id) 
        {
            var gameReview = gameReviews.Find(x => x.Id == id);
            gameReview.Print();
        }

        public void GetAll() 
        {
            foreach(var game in gameReviews)
            {
                game.Print();
            }
        }

        public GameReview EnterData1()
        {
            GameReview gameReview = new GameReview();

            Console.Write("User name: ");
            gameReview.UserName = Console.ReadLine();

            Console.Write("Game name: ");
            gameReview.GameName = Console.ReadLine();

            Console.Write("Mark: ");
            gameReview.Mark = Convert.ToInt32(Console.ReadLine());

            Console.Write("Review: ");
            gameReview.Review = Console.ReadLine();

            return gameReview;

        }
        public GameReview EnterData()
        {
            GameReview gameReview = new GameReview();

            Random rnd = new Random();
            gameReview.Id = rnd.Next(1, 100);

            Console.Write("User name: ");
            gameReview.UserName = Console.ReadLine();

            Console.Write("Game name: ");
            gameReview.GameName = Console.ReadLine();

            Console.Write("Mark: ");
            gameReview.Mark = Convert.ToInt32(Console.ReadLine());

            Console.Write("Review: ");
            gameReview.Review = Console.ReadLine();

            return gameReview;

        }

        public int EnterId()
        {
            Console.Write("Enter ID");
            int id = Convert.ToInt32(Console.ReadLine());

            return id;
        }

        public void DoThings(int menuChoise)
        {
            GameReview gameReview;
            int id;

            switch (menuChoise)
            {
                case 1:
                    gameReview = EnterData();
                    Add(gameReview);
                    break;
                case 2:
                    id = EnterId();
                    Delete(id);
                    break;
                case 3:
                    GetAll();
                    break;
                case 4:
                    id = EnterId();
                    Get(id);
                    break;
                case 5: 
                    id = EnterId();

                    Update(id);
                    break;
                case 0:
                    Environment.Exit(0);
                    break;
            }
        }


        //public void AddGameReview()
        //{
        //    /*Random rnd = new Random();
        //    int id = rnd.Next(1, 100);

        //    string gameName = "Game - Squares";
        //    string userName = "Username is Vova";
          
        //    Console.WriteLine("Enter mark you think game deserved: ");
        //    int mark = Console.Read();
        //    Console.WriteLine("Thanks for your mark {0}", mark);
         
        //    AnoutherFeedback:

        //    Console.Write("Give us a feedback: ");
        //    string myReview = Console.ReadLine();
        //    Console.WriteLine("Your feed back :{0},your review`s id :{1}", myReview, id);

        //    GameReview review = new GameReview
        //    {
        //        Id = id,
        //        GameName = gameName,
        //        UserName = userName,
        //        Mark = mark,
        //        Review = myReview
        //    };
        //    Console.WriteLine("Thanks for your mark {0}", mark);
        //    gameReviews.Add(review);

            
      
        //    Console.WriteLine("Enter 1 if you want to leave anouther review,2 to continue work of the program,and 0 to exit program.");

        //    int n;
        //    for (; ; )
        //    {
        //        if (Int32.TryParse(Console.ReadLine(), out n))
        //        {
        //            if (n <= -1) { n = 0; }
        //            else if (n >= 3) { n = 1; }
                    

        //            Console.WriteLine("good number");
        //        }

        //        switch (n)
        //        {
        //            case 0:
        //                {
        //                    Console.WriteLine("Thanks for leaving a review");
        //                    return;
        //                }
        //            case 1:
        //                {
        //                    goto AnoutherFeedback;
        //                }
        //            case 2:
        //                {
        //                    return ;
        //                }
                        
        //        }
            
        //    }*/
        //}

        //public void DeleteGameReview()
        //{

        //     int Del_Value = 0;

        //    Console.WriteLine("Lets delete your first review");

        //    for(; ; )
        //    {
                


        //        gameReviews.RemoveAt(Del_Value);
        //        Del_Value += 1;


        //    }



        //}

        //public void ShowGameReview()
        //{
        //    Console.WriteLine("Hey");



        //}

        //public void ShowGameReviews()
        //{

        //    Console.WriteLine("Hey");


        //}

    }
}
