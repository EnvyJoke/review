﻿using System;

namespace Game_Review_basics
{
    public class GameReview
    {
        private int _mark;

        public int Id { get; set; }
        public string GameName { get; set; }
        public string UserName { get; set; }

        public int Mark {
            set
            {
                if (value < 1)
                {
                    _mark = 1;
                }
                else if (value > 10)
                {
                    _mark = 10;
                }
            }
            get
            {
                return _mark;
            }

        }

        public string Review { get; set; }

        public GameReview() { }

        public void Print()
        {
            Console.WriteLine(Id);
            Console.WriteLine(GameName);
            Console.WriteLine(UserName);        
            Console.WriteLine(Mark);
            Console.WriteLine(Review);
        }
    }
}
