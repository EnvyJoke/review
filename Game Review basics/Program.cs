﻿using System;


namespace Game_Review_basics
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Hey,thanks for leaving a review,its very important for us <3");
            GameReviewService gameReviewService = new GameReviewService();

            int menuChoise;
            

            while(true)
            {
                Console.WriteLine("1 - Add \n" +
                "2 - Delete \n" +
                "3 - Get all \n" +
                "4 - Get one by Id \n" +
                "5 - Update \n" +
                "0 - Exit\n" +
                "-----------------------");
                menuChoise = Convert.ToInt32(Console.ReadLine());
                
                    if (menuChoise < 0)
                    {
                        menuChoise = 0;
                    }
                    else if (menuChoise > 5)
                    {
                        menuChoise = 5;
                    }
                    
                gameReviewService.DoThings(menuChoise);
            }
        }


        
    }
}
